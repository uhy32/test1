#include "sierrachart.h"
SCDLLName("uhy")

/*"Uhy Trading OCO Order Example"*/
SCSFExport scsf_UhyACSILTradingOCOExample(SCStudyInterfaceRef sc)
{
	//Define references to the Subgraphs and Inputs for easy reference

	SCInputRef Enabled = sc.Input[0];
	SCInputRef SendOrdersToTradeService = sc.Input[1];
	SCInputRef OrderQuantity = sc.Input[2];
	SCInputRef PositiveTickSize = sc.Input[3];
	SCInputRef NegativeTickSize = sc.Input[4];
	SCInputRef Target1Offset = sc.Input[5];
	SCInputRef Stop1Offset = sc.Input[6];
	SCInputRef Target1Offset_2 = sc.Input[7];
	SCInputRef Stop1Offset_2 = sc.Input[8];
	//int& xxxx = sc.GetPersistentInt(100);//Positive Tick Size




	if (sc.SetDefaults)
	{
		// Set the study configuration and defaults.

		sc.GraphName = "Uhy Trading OCO Order Example";

		Enabled.Name = "Enabled";
		Enabled.SetYesNo(1);
		Enabled.SetDescription("This input enables the study and allows it to function. Otherwise, it does nothing.");

		SendOrdersToTradeService.Name = "Send order to trade service";
		SendOrdersToTradeService.SetYesNo(1);
		SendOrdersToTradeService.SetDescription("This input enables send order to trade service.");

		OrderQuantity.Name = "Order Quantity";
		OrderQuantity.SetInt(1);
		OrderQuantity.SetDescription("This input define Order Quantity.");

		PositiveTickSize.Name = "Positive Tick Size";
		PositiveTickSize.SetInt(4);
		PositiveTickSize.SetDescription("This input define Positive Tick Size.");

		NegativeTickSize.Name = "Negative Tick Size";
		NegativeTickSize.SetInt(4);
		NegativeTickSize.SetDescription("This input define Negative Tick Size.");

		Target1Offset.Name = "Target 1 offset";
		Target1Offset.SetInt(20);
		Target1Offset.SetDescription("This input define Target 1 offset.");

		Stop1Offset.Name = "Stop 1 offset";
		Stop1Offset.SetInt(10);
		Stop1Offset.SetDescription("This input define Stop 1 offset.");


		Target1Offset_2.Name = "Target 1 offset 2";
		Target1Offset_2.SetInt(20);
		Target1Offset_2.SetDescription("This input define Target 1 offset 2.");

		Stop1Offset_2.Name = "Stop 1 offset 2";
		Stop1Offset_2.SetInt(10);
		Stop1Offset_2.SetDescription("This input define Stop 1 offset 2.");

		// This is false by default. Orders will go to the simulation system always.
		sc.SendOrdersToTradeService = true; //uhy-false
		sc.AllowMultipleEntriesInSameDirection = false;
		sc.MaximumPositionAllowed = 10;
		sc.SupportReversals = false;
		sc.AllowOppositeEntryWithOpposingPositionOrOrders = false;
		sc.SupportAttachedOrdersForTrading = false;
		sc.CancelAllOrdersOnEntriesAndReversals = false;
		sc.AllowEntryWithWorkingOrders = false;
		sc.CancelAllWorkingOrdersOnExit = true;
		sc.AllowOnlyOneTradePerBar = false;



		//This needs to be set to true when a trading study uses trading functions.
		sc.MaintainTradeStatisticsAndTradesData = true;

		sc.AutoLoop = false;

		sc.GraphRegion = 0;

		sc.FreeDLL = 1;

		return;
	}


	//These must be outside of the sc.SetDefaults code block
	sc.SupportTradingScaleIn = 0;
	sc.SupportTradingScaleOut = 0;


	if (!Enabled.GetYesNo())
		return;

	if (!SendOrdersToTradeService.GetYesNo())
		return;

	if (!OrderQuantity.GetInt())
		return;

	if (!PositiveTickSize.GetInt())
		return;

	if (!NegativeTickSize.GetInt())
		return;

	if (!Target1Offset.GetInt())
		return;

	if (!Stop1Offset.GetInt())
		return;

	if (!Target1Offset_2.GetInt())
		return;

	if (!Stop1Offset_2.GetInt())
		return;

	// Do not run on a full calculation.
	if (sc.IsFullRecalculation)
		return;

	float Last = sc.BaseDataIn[SC_LAST][sc.Index];

	s_SCPositionData PositionData;
	sc.GetTradePosition(PositionData);

	if (PositionData.PositionQuantity != 0 || PositionData.WorkingOrdersExist)
		return;

	s_SCNewOrder NewOrder;
	NewOrder.OrderQuantity = OrderQuantity.GetInt();
	NewOrder.OrderType = SCT_ORDERTYPE_OCO_BUY_STOP_SELL_STOP; //SCT_ORDERTYPE_OCO_BUY_STOP_SELL_STOP
	NewOrder.TimeInForce = SCT_TIF_GOOD_TILL_CANCELED;
	

	NewOrder.Price1 = Last + PositiveTickSize.GetInt() * sc.TickSize;//10
	NewOrder.Price2 = Last - NegativeTickSize.GetInt() * sc.TickSize;//10

	// Optional: Add target and stop Attached Orders to each stop in the OCO pair.
	NewOrder.AttachedOrderTarget1Type = SCT_ORDERTYPE_LIMIT;
	NewOrder.Target1Offset = Target1Offset.GetInt() * sc.TickSize;//8

	NewOrder.AttachedOrderStop1Type = SCT_ORDERTYPE_STOP;//SCT_ORDERTYPE_STOP
	NewOrder.Stop1Offset = Stop1Offset.GetInt() * sc.TickSize;//4

	// Optional: Use different Attached Orders for the 2nd order in the OCO pair.

	NewOrder.Target1Offset_2 = Target1Offset_2.GetInt() * sc.TickSize;//8
	NewOrder.Stop1Offset_2 = Stop1Offset_2.GetInt() * sc.TickSize;//8

	NewOrder.Stop1Price_2 = NewOrder.Price2 + 2;

	if (sc.SubmitOCOOrder(NewOrder) > 0)
	{
		int BuyStopOrder1ID = NewOrder.InternalOrderID;
		int SellStopOrder2ID = NewOrder.InternalOrderID2;
	}
}

/*"UHY Trading Example: Using Moving Average and Target and Stop"*/
SCSFExport scsf_Uhy2TradingExample(SCStudyInterfaceRef sc)
{
	//Define references to the Subgraphs and Inputs for easy reference
	SCSubgraphRef BuyEntrySubgraph = sc.Subgraph[0];
	SCSubgraphRef BuyExitSubgraph = sc.Subgraph[1];
	SCSubgraphRef SellEntrySubgraph = sc.Subgraph[2];
	SCSubgraphRef SellExitSubgraph = sc.Subgraph[3];
	SCSubgraphRef SimpMovAvgSubgraph = sc.Subgraph[4];

	SCInputRef Enabled = sc.Input[0];
	SCInputRef TargetValue = sc.Input[1];
	SCInputRef StopValue = sc.Input[2];


	if (sc.SetDefaults)
	{
		// Set the study configuration and defaults.

		sc.GraphName = "Uhy Trading Example: Using Moving Average and Target and Stop";

		BuyEntrySubgraph.Name = "Buy Entry";
		BuyEntrySubgraph.DrawStyle = DRAWSTYLE_ARROW_UP;
		BuyEntrySubgraph.PrimaryColor = RGB(0, 255, 0);
		BuyEntrySubgraph.LineWidth = 2;
		BuyEntrySubgraph.DrawZeros = false;

		BuyExitSubgraph.Name = "Buy Exit";
		BuyExitSubgraph.DrawStyle = DRAWSTYLE_ARROW_DOWN;
		BuyExitSubgraph.PrimaryColor = RGB(255, 128, 128);
		BuyExitSubgraph.LineWidth = 2;
		BuyExitSubgraph.DrawZeros = false;

		SellEntrySubgraph.Name = "Sell Entry";
		SellEntrySubgraph.DrawStyle = DRAWSTYLE_ARROW_DOWN;
		SellEntrySubgraph.PrimaryColor = RGB(255, 0, 0);
		SellEntrySubgraph.LineWidth = 2;
		SellEntrySubgraph.DrawZeros = false;

		SellExitSubgraph.Name = "Sell Exit";
		SellExitSubgraph.DrawStyle = DRAWSTYLE_ARROW_UP;
		SellExitSubgraph.PrimaryColor = RGB(128, 255, 128);
		SellExitSubgraph.LineWidth = 2;
		SellExitSubgraph.DrawZeros = false;

		SimpMovAvgSubgraph.Name = "Simple Moving Average";
		SimpMovAvgSubgraph.DrawStyle = DRAWSTYLE_LINE;
		SimpMovAvgSubgraph.DrawZeros = false;

		Enabled.Name = "Enabled";
		Enabled.SetYesNo(0);

		TargetValue.Name = "Target Value";
		TargetValue.SetFloat(2.0f);

		StopValue.Name = "Stop Value";
		StopValue.SetFloat(1.0f);

		sc.StudyDescription = "This study function is an example of how to use the ACSIL Trading Functions. This function will display a simple moving average and perform a Buy Entry when the Last price crosses the moving average from below and a Sell Entry when the Last price crosses the moving average from above. A new entry cannot occur until the Target or Stop has been hit. When an order is sent, a corresponding arrow will appear on the chart to show that an order was sent. This study will do nothing until the Enabled Input is set to Yes.";

		sc.AutoLoop = 1;
		sc.GraphRegion = 0;

		//Any of the following variables can also be set outside and below the sc.SetDefaults code block

		sc.AllowMultipleEntriesInSameDirection = false;
		sc.MaximumPositionAllowed = 10;
		sc.SupportReversals = false;

		// This is false by default. Orders will go to the simulation system always.
		//sc.SendOrdersToTradeService = true;//uhy-false
		sc.AllowOppositeEntryWithOpposingPositionOrOrders = false;
		sc.SupportAttachedOrdersForTrading = false;
		sc.CancelAllOrdersOnEntriesAndReversals = true;
		sc.AllowEntryWithWorkingOrders = false;
		sc.CancelAllWorkingOrdersOnExit = false;

		// Only 1 trade for each Order Action type is allowed per bar.
		sc.AllowOnlyOneTradePerBar = true;

		//This needs to be set to true when a trading study uses trading functions.
		sc.MaintainTradeStatisticsAndTradesData = true;


		return;
	}

	if (!Enabled.GetYesNo())
		return;

	SCFloatArrayRef Last = sc.Close;

	// Calculate the moving average
	sc.SimpleMovAvg(Last, SimpMovAvgSubgraph, sc.Index, 10);


	if (sc.IsFullRecalculation)
		return;

	// Get the Trade Position data to be used for position exit processing.
	s_SCPositionData PositionData;
	sc.GetTradePosition(PositionData);


	float LastTradePrice = sc.Close[sc.Index];

	int& r_BuyEntryInternalOrderID = sc.GetPersistentInt(1);

	// Create an s_SCNewOrder object. 
	s_SCNewOrder NewOrder;
	NewOrder.OrderQuantity = 1;
	NewOrder.OrderType = SCT_ORDERTYPE_MARKET;
	NewOrder.TextTag = "Trading example tag";
	//NewOrder.Symbol = "Test";
	NewOrder.TimeInForce = SCT_TIF_GOOD_TILL_CANCELED;

	int Result = 0;

	//Optional: Check if within allowed time range. In this example we will use 10:00 through 14:00. This is according to the time zone of the chart.
	//int BarTime = sc.BaseDateTimeIn[sc.Index].GetTime();
	//bool TradingAllowed = BarTime >= HMS_TIME(10,  0, 0) && BarTime  < HMS_TIME(14,  0, 0);

	bool TradingAllowed = true;

	// Buy when the last price crosses the moving average from below.
	if (TradingAllowed && sc.CrossOver(Last, SimpMovAvgSubgraph) == CROSS_FROM_BOTTOM && sc.GetBarHasClosedStatus() == BHCS_BAR_HAS_CLOSED)
	{
		Result = (int)sc.BuyEntry(NewOrder);
		//Result = sc.BuyOrder(NewOrder);
		//If there has been a successful order entry, then draw an arrow at the low of the bar.
		if (Result > 0)
		{
			r_BuyEntryInternalOrderID = NewOrder.InternalOrderID;
			SCString InternalOrderIDNumberString;
			InternalOrderIDNumberString.Format("BuyEntry Internal Order ID: %d", r_BuyEntryInternalOrderID);
			sc.AddMessageToLog(InternalOrderIDNumberString, 0);

			BuyEntrySubgraph[sc.Index] = sc.Low[sc.Index];
		}
	}


	// When there is a long position AND the Last price is less than the price the Buy Entry was filled at minus Stop Value, OR there is a long position AND the Last price is greater than the price the Buy Entry was filled at plus the Target Value.
	else if (PositionData.PositionQuantity > 0
		&& (LastTradePrice <= PositionData.AveragePrice - StopValue.GetFloat() ||
			LastTradePrice >= PositionData.AveragePrice + TargetValue.GetFloat()))
	{
		Result = (int)sc.BuyExit(NewOrder);

		//If there has been a successful order entry, then draw an arrow at the high of the bar.
		if (Result > 0)
		{
			BuyExitSubgraph[sc.Index] = sc.High[sc.Index];
		}
	}


	// Sell when the last price crosses the moving average from above.
	else if (TradingAllowed && sc.CrossOver(Last, SimpMovAvgSubgraph) == CROSS_FROM_TOP && sc.GetBarHasClosedStatus() == BHCS_BAR_HAS_CLOSED)
	{
		Result = (int)sc.SellEntry(NewOrder);

		// If there has been a successful order entry, then draw an arrow at the high of the bar.
		if (Result > 0)
		{
			SellEntrySubgraph[sc.Index] = sc.High[sc.Index];
		}
	}

	// When there is a short position AND the Last price is greater than the price the Sell Entry was filled at plus the Stop Value, OR there is a short position AND the Last price is less than the price the Sell Entry was filled at minus the Target Value.
	else if (PositionData.PositionQuantity < 0
		&& (LastTradePrice >= PositionData.AveragePrice + StopValue.GetFloat() ||
			LastTradePrice <= PositionData.AveragePrice - TargetValue.GetFloat()))
	{
		Result = (int)sc.SellExit(NewOrder);

		// If there has been a successful order entry, then draw an arrow at the low of the bar.
		if (Result > 0)
		{
			SellExitSubgraph[sc.Index] = sc.Low[sc.Index];
		}
	}

	//Example to check the status of the order using the Internal Order ID remembered in a reference to a persistent variable.
	if (r_BuyEntryInternalOrderID != 0)
	{
		s_SCTradeOrder TradeOrder;
		sc.GetOrderByOrderID(r_BuyEntryInternalOrderID, TradeOrder);

		// Order has been found.
		if (TradeOrder.InternalOrderID == r_BuyEntryInternalOrderID)
		{
			bool IsOrderFilled = TradeOrder.OrderStatusCode == SCT_OSC_FILLED;
			double FillPrice = TradeOrder.LastFillPrice;
		}
	}
}
